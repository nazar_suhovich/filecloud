package com.midgets.fileclouddemo.login;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "commands")
public class Commands {
    @Element(name = "command")
    private Command command;

    public Command getCommand () {
        return command;
    }

    public void setCommand (Command command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "[command = " + command + "]";
    }
}
