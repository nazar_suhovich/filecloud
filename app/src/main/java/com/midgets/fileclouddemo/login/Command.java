package com.midgets.fileclouddemo.login;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "command")
public class Command {
    @Element(name = "message", required = false)
    private String message;
    @Element(name = "result")
    private int result;
    @Element(name = "type")
    private String type;

    public String getMessage () {
        return message;
    }

    public void setMessage (String message) {
        this.message = message;
    }

    public int getResult () {
        return result;
    }

    public void setResult (int result) {
        this.result = result;
    }

    public String getType () {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "[message = " + message + ", \nresult = " + result + ", \ntype = " + type + "]";
    }
}
