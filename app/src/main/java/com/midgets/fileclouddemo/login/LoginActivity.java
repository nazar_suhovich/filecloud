package com.midgets.fileclouddemo.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.Toast;

import com.midgets.fileclouddemo.Consts;
import com.midgets.fileclouddemo.R;
import com.midgets.fileclouddemo.TinyDB;
import com.midgets.fileclouddemo.network.Api;
import com.midgets.fileclouddemo.network.RetrofitBuilder;
import com.midgets.fileclouddemo.search.SearchActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText mLoginField;
    private EditText mPasswordField;
    private EditText urlField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mLoginField = (EditText) findViewById(R.id.userField);
        mPasswordField = (EditText) findViewById(R.id.passField);
        urlField = (EditText) findViewById(R.id.urlField);
        findViewById(R.id.submitButton).setOnClickListener(view -> login());
        if (TinyDB.getInstance(this).getBoolean(Consts.LOGGED, false)) {
            startActivity(new Intent(this, SearchActivity.class));
            finish();
        }
    }

    private void login() {
        String login = mLoginField.getText().toString().trim();
        if (TextUtils.isEmpty(login)) {
            Toast.makeText(LoginActivity.this, "Empty login", Toast.LENGTH_SHORT).show();
            return;
        }
        String password = mPasswordField.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(LoginActivity.this, "Empty password", Toast.LENGTH_SHORT).show();
            return;
        }
        String url = urlField.getText().toString().trim();
        if (TextUtils.isEmpty(url)) {
            Toast.makeText(LoginActivity.this, "Empty url", Toast.LENGTH_SHORT).show();
            return;
        } else {
            if (!url.startsWith("http")) {
                Toast.makeText(LoginActivity.this, "Wrong url, must start with http", Toast.LENGTH_SHORT).show();
                return;
            } else if (url.endsWith("/")) {
                Toast.makeText(LoginActivity.this, "Url must be without / at the end", Toast.LENGTH_SHORT).show();
                return;
            } else if (!Patterns.WEB_URL.matcher(url).matches()) {
                Toast.makeText(LoginActivity.this, "Url is not valid", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        Call<Commands> call = RetrofitBuilder.getApi().login(url + "/core/loginguest", Api.U_AGENT, Api.XML_TYPE, login, password);
        call.enqueue(new Callback<Commands>() {
            @Override
            public void onResponse(Call<Commands> call, Response<Commands> response) {
                Log.d(TAG, "onResponse: " + response.code());
                Log.d(TAG, "onResponse: " + response.body().toString());
                if (response.body().getCommand().getResult() == 1) {
                    okhttp3.Headers headers = response.headers();
                    List<String> values = headers.values("Set-Cookie");
                    StringBuilder sb = new StringBuilder();
                    boolean isInserted = false;
                    for (String string : values) {
                        if (string.contains("tonido-login")) {
                            if (isInserted) {
                                sb.append("; ");
                            }
                            String[] parts = string.split(";");
                            String token = parts[0].trim();
                            sb.append(token);
                            isInserted = true;
                        }
                    }

                    Log.d(TAG, "onResponse: " + sb.toString());
                    TinyDB.getInstance(LoginActivity.this).putString(Consts.TOKEN, sb.toString());
                    TinyDB.getInstance(LoginActivity.this).putBoolean(Consts.LOGGED, true);
                    TinyDB.getInstance(LoginActivity.this).putString(Consts.DOMEN, url);
                    startActivity(new Intent(LoginActivity.this, SearchActivity.class));
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, response.body().getCommand().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Commands> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
            }
        });
    }
}
