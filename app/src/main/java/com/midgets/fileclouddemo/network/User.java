package com.midgets.fileclouddemo.network;

public class User {
    public final String userid;
    public final String password;

    public User(String userid, String password) {
        this.userid = userid;
        this.password = password;
    }
}
