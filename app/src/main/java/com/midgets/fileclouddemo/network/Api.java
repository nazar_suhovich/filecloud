package com.midgets.fileclouddemo.network;

import com.midgets.fileclouddemo.login.Commands;
import com.midgets.fileclouddemo.search.Entries;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {
    int OK = 200;
    int UPDATED = 201;
    int NOT_FOUND = 404;
    int UNATHORIZED = 401;

    //String BASE_URL = "http://demo.getfilecloud.com/core/";
    String BASE_URL = "http://demo.getfilecloud.com/";
    String C_TYPE = "text/xml;charset=UTF-8";
    String XML_TYPE = "application/x-www-form-urlencoded";
    String U_AGENT = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2";
    String JSON_TYPE = "application/json";

    @POST
    Call<Commands> login(@Url String domen, @Header("User-Agent") String agent, @Header("Content-Type") String type, @Query("userid") String userid, @Query("password") String password);

    @GET
    Call<Entries> getRootFolder(@Url String domen, @Header("User-Agent") String agent, @Header("Cookie") String token);

    @GET
    Call<Entries> getFolder(@Url String domen, @Header("User-Agent") String agent, @Header("Cookie") String token, @Query("path") String path);
}
