package com.midgets.fileclouddemo.search;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.midgets.fileclouddemo.Consts;
import com.midgets.fileclouddemo.R;
import com.midgets.fileclouddemo.TinyDB;
import com.midgets.fileclouddemo.login.LoginActivity;
import com.midgets.fileclouddemo.network.Api;
import com.midgets.fileclouddemo.network.RetrofitBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "SearchActivity";
    private EditText searchField;
    private RecyclerView fileList;
    private String token = TinyDB.getInstance(this).getString(Consts.TOKEN);
    private String domen = TinyDB.getInstance(this).getString(Consts.DOMEN);
    private List<FileItem> mData = new ArrayList<>();

    private int mFolderCount;
    private int mTotalCount;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchField = (EditText) findViewById(R.id.searchField);
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                search();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        fileList = (RecyclerView) findViewById(R.id.fileList);
        fileList.setHasFixedSize(true);
        fileList.setLayoutManager(new LinearLayoutManager(this));
        findViewById(R.id.searchButton).setOnClickListener(view -> search());
        refresh();
    }

    private void search() {
        String query = searchField.getText().toString().trim().toLowerCase();
        List<FileItem> filtered = new ArrayList<>();
        for (FileItem item : mData) {
            if (item.getName().toLowerCase().contains(query)) {
                filtered.add(item);
            }
        }
        if (query.matches("")) filtered = mData;
        FilesRecyclerAdapter adapter = new FilesRecyclerAdapter(this, filtered, false);
        fileList.setAdapter(adapter);
        if (filtered.size() == 0) {
            Toast.makeText(SearchActivity.this, "Nothing found!", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadRoot() {
        Call<Entries> call = RetrofitBuilder.getApi().getRootFolder(domen + "/core/getfilelist", Api.U_AGENT, token);
        call.enqueue(new Callback<Entries>() {
            @Override
            public void onResponse(Call<Entries> call, Response<Entries> response) {
                int size = response.body().getEntry().size();
                if (response.code() == Api.OK && size > 0) {
                    for (Entry entry : response.body().getEntry()) {
                        FileItem fileItem = new FileItem(entry.getType(), entry.getName(), entry.getPath());
                        addToList(fileItem);
                    }
                    for (Entry entry : response.body().getEntry()) {
                        if (entry.getType().matches(FileItem.FOLDER)) {
                            mFolderCount +=1;
                            loadFolder(entry.getPath());
                        } else {
                            mTotalCount += 1;
                        }
                    }
                }
                checkProgress();
            }

            @Override
            public void onFailure(Call<Entries> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
                checkProgress();
            }
        });
    }

    private void addToList(FileItem fileItem) {
        int pos = getPosition(fileItem.getPath());
        if (pos == -1) mData.add(fileItem);
        else mData.add(pos, fileItem);
    }

    private int getPosition(String path) {
        if (mData.size() == 0) return 0;
        int position = -1;
        for (FileItem data : mData) {
            int comp = path.compareTo(data.getPath());
            if (comp <= 0) {
                position = mData.indexOf(data);
                break;
            }
        }
        return position;
    }

    private void checkProgress() {
        if (mFolderCount <= 0) {
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
            }
            Toast.makeText(SearchActivity.this, "You have " + mTotalCount + " files in Cloud", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadFolder(String path) {
        Call<Entries> call = RetrofitBuilder.getApi().getFolder(domen + "/core/getfilelist", Api.U_AGENT, token, path);
        call.enqueue(new Callback<Entries>() {
            @Override
            public void onResponse(Call<Entries> call, Response<Entries> response) {
                mFolderCount -= 1;
                if (response.code() == Api.OK) {
                    int size = response.body().getEntry().size();
                    if (size > 0) {
                        for (Entry entry : response.body().getEntry()) {
                            FileItem fileItem = new FileItem(entry.getType(), entry.getName(), entry.getPath());
                            addToList(fileItem);
                        }
                        for (Entry entry : response.body().getEntry()) {
                            if (entry.getType().matches(FileItem.FOLDER)) {
                                mFolderCount += 1;
                                loadFolder(entry.getPath());
                            } else {
                                mTotalCount += 1;
                            }
                        }
                    }
                }
                checkProgress();
            }

            @Override
            public void onFailure(Call<Entries> call, Throwable t) {
                Log.d(TAG, "onFailure2: " + t.getLocalizedMessage());
                mFolderCount -= 1;
                checkProgress();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.action_logout) {
            showDialog();
        }
        if (item.getItemId() == R.id.action_refresh) {
            refresh();
        }
        return true;
    }

    private void refresh() {
        mData.clear();
        mFolderCount = 0;
        mTotalCount = 0;
        mDialog = ProgressDialog.show(this, "Setting up search engine", "Please wait...", false, false);
        loadRoot();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You really want to logout?");
        builder.setNegativeButton("No", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        builder.setPositiveButton("Yes, logout", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            logOut();
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void logOut() {
        TinyDB.getInstance(this).remove(Consts.TOKEN);
        TinyDB.getInstance(this).remove(Consts.DOMEN);
        TinyDB.getInstance(this).remove(Consts.LOGGED);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
