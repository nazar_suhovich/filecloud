package com.midgets.fileclouddemo.search;

public class FileItem {

    public static final String FOLDER = "dir";
    public static final String FILE = "file";

    private String type;
    private String name;
    private String path;

    public FileItem(String type, String name, String path) {
        this.type = type;
        this.name = name;
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }
}
