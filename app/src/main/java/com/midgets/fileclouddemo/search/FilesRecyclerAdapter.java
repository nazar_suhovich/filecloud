package com.midgets.fileclouddemo.search;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.midgets.fileclouddemo.R;
import com.midgets.fileclouddemo.databinding.FileItemLayoutBinding;

import java.util.List;

public class FilesRecyclerAdapter extends RecyclerView.Adapter<FilesRecyclerAdapter.MyViewHolder> {

    private Context mContext;
    private List<FileItem> mDataList;
    private boolean isClickable;

    public FilesRecyclerAdapter(Context context, List<FileItem> dataItemList, boolean clickable) {
        this.mContext = context;
        this.mDataList = dataItemList;
        this.isClickable = clickable;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new MyViewHolder(DataBindingUtil.inflate(inflater, R.layout.file_item_layout, parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FileItem item = mDataList.get(position);
        holder.binding.setItem(item);
    }

    @Override
    public int getItemCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        FileItemLayoutBinding binding;

        public MyViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public FileItem getItem(int position) {
        return mDataList.get(position);
    }


    @BindingAdapter({"app:loadIcon"})
    public static void loadIcon(ImageView imageView, String v) {
        if (v.matches(FileItem.FILE)) {
            imageView.setImageResource(R.drawable.ic_insert_drive_file_black_24dp);
        } else {
            imageView.setImageResource(R.drawable.ic_folder_black_24dp);
        }
    }
}
