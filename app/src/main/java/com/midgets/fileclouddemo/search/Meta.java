package com.midgets.fileclouddemo.search;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "meta")
public class Meta {
    @Element(required = false)
    private int isroot;

    @Element(required = false)
    private String parentpath;

    @Element(required = false)
    private int total;

    @Element(required = false)
    private String realpath;

    @Element(required = false)
    private int canupload;

    @Element(required = false)
    private int isshareable;

    @Element(required = false)
    private int candownload;

    @Element(required = false)
    private int showshareoption;

    public int getIsroot() {
        return isroot;
    }

    public void setIsroot(int isroot) {
        this.isroot = isroot;
    }

    public String getParentpath() {
        return parentpath;
    }

    public void setParentpath(String parentpath) {
        this.parentpath = parentpath;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getRealpath() {
        return realpath;
    }

    public void setRealpath(String realpath) {
        this.realpath = realpath;
    }

    public int getCanupload() {
        return canupload;
    }

    public void setCanupload(int canupload) {
        this.canupload = canupload;
    }

    public int getIsshareable() {
        return isshareable;
    }

    public void setIsshareable(int isshareable) {
        this.isshareable = isshareable;
    }

    public int getCandownload() {
        return candownload;
    }

    public void setCandownload(int candownload) {
        this.candownload = candownload;
    }

    public int getShowshareoption() {
        return showshareoption;
    }

    public void setShowshareoption(int showshareoption) {
        this.showshareoption = showshareoption;
    }

    @Override
    public String toString() {
        return "[isroot = "+isroot+", parentpath = "+parentpath+"]";
    }
}
