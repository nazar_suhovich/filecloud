package com.midgets.fileclouddemo.search;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root
public class Entry {

    @Element(required = false)
    private int canrename;

    @Element(required = false)
    private int canupload;

    @Element(required = false)
    private int candownload;

    @Element(required = false)
    private String type;

    @Element(required = false)
    private String ext;

    @Element(required = false)
    private String isshared;

    @Element(required = false)
    private String order;

    @Element(required = false)
    private int isroot;

    @Element(required = false)
    private String name;

    @Element(required = false)
    private String path;

    @Element(required = false)
    private int favoriteid;

    @Element(required = false)
    private String modifiedepoch;

    @Element(required = false)
    private long fullsize;

    @Element(required = false)
    private String fullfilename;

    @Element(required = false)
    private int isshareable;

    @Element(required = false)
    private int showlockunlock;

    @Element(required = false)
    private int showshareoption;

    @Element(required = false)
    private int favoritelistid;

    @Element(required = false)
    private String size;

    @Element(required = false)
    private String modified;

    @Element(required = false)
    private int canfavorite;

    @Element(required = false)
    private int showprev;

    @Element(required = false)
    private int showquickedit;

    @Element(required = false)
    private int issyncable;

    @Element(required = false)
    private String dirpath;

    @Element(required = false)
    private int locked;

    public int getCanrename() {
        return canrename;
    }

    public void setCanrename(int canrename) {
        this.canrename = canrename;
    }

    public int getCanupload() {
        return canupload;
    }

    public void setCanupload(int canupload) {
        this.canupload = canupload;
    }

    public int getCandownload() {
        return candownload;
    }

    public void setCandownload(int candownload) {
        this.candownload = candownload;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getIsshared() {
        return isshared;
    }

    public void setIsshared(String isshared) {
        this.isshared = isshared;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getIsroot() {
        return isroot;
    }

    public void setIsroot(int isroot) {
        this.isroot = isroot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getFavoriteid() {
        return favoriteid;
    }

    public void setFavoriteid(int favoriteid) {
        this.favoriteid = favoriteid;
    }

    public String getModifiedepoch() {
        return modifiedepoch;
    }

    public void setModifiedepoch(String modifiedepoch) {
        this.modifiedepoch = modifiedepoch;
    }

    public long getFullsize() {
        return fullsize;
    }

    public void setFullsize(long fullsize) {
        this.fullsize = fullsize;
    }

    public String getFullfilename() {
        return fullfilename;
    }

    public void setFullfilename(String fullfilename) {
        this.fullfilename = fullfilename;
    }

    public int getIsshareable() {
        return isshareable;
    }

    public void setIsshareable(int isshareable) {
        this.isshareable = isshareable;
    }

    public int getShowlockunlock() {
        return showlockunlock;
    }

    public void setShowlockunlock(int showlockunlock) {
        this.showlockunlock = showlockunlock;
    }

    public int getShowshareoption() {
        return showshareoption;
    }

    public void setShowshareoption(int showshareoption) {
        this.showshareoption = showshareoption;
    }

    public int getFavoritelistid() {
        return favoritelistid;
    }

    public void setFavoritelistid(int favoritelistid) {
        this.favoritelistid = favoritelistid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public int getCanfavorite() {
        return canfavorite;
    }

    public void setCanfavorite(int canfavorite) {
        this.canfavorite = canfavorite;
    }

    public int getShowprev() {
        return showprev;
    }

    public void setShowprev(int showprev) {
        this.showprev = showprev;
    }

    public int getShowquickedit() {
        return showquickedit;
    }

    public void setShowquickedit(int showquickedit) {
        this.showquickedit = showquickedit;
    }

    public int getIssyncable() {
        return issyncable;
    }

    public void setIssyncable(int issyncable) {
        this.issyncable = issyncable;
    }

    public String getDirpath() {
        return dirpath;
    }

    public void setDirpath(String dirpath) {
        this.dirpath = dirpath;
    }

    public int getLocked() {
        return locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }
}
