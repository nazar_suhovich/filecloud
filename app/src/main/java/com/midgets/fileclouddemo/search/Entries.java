package com.midgets.fileclouddemo.search;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "entries")
public class Entries {
    @ElementList(entry = "entry", required = false, inline=true)
    private List<Entry> entry = new ArrayList<>();

    @Element(required = false)
    private Meta meta;

    public List<Entry> getEntry () {
        return entry;
    }

    public void setEntry (List<Entry> entry) {
        this.entry = entry;
    }

    public Meta getMeta () {
        return meta;
    }

    public void setMeta (Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "[entry = "+entry+", meta = "+meta+"]";
    }
}
